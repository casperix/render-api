package casper.render.material

data class Material(
		val albedo: ColorReference? = null,
		val ambient: ColorReference? = null,
		val bump: ColorReference? = null,
		val emissive: ColorReference? = null,
		val metallic: FloatReference? = null,
		val opacity: FloatReference? = null,
		val reflection: ColorReference? = null,
		val roughness: FloatReference? = null,
		val wireFrame: Boolean = false
) {
	private var _hashCode = {
		var result = albedo?.hashCode() ?: 0
		result = 31 * result + (ambient?.hashCode() ?: 0)
		result = 31 * result + (bump?.hashCode() ?: 0)
		result = 31 * result + (emissive?.hashCode() ?: 0)
		result = 31 * result + (metallic?.hashCode() ?: 0)
		result = 31 * result + (opacity?.hashCode() ?: 0)
		result = 31 * result + (reflection?.hashCode() ?: 0)
		result = 31 * result + (roughness?.hashCode() ?: 0)
		result = 31 * result + wireFrame.hashCode()
		result
	}()

	override fun hashCode(): Int {
		return _hashCode
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (other == null || this::class != other::class) return false

		other as Material

		if (_hashCode != other._hashCode) return false
		if (albedo != other.albedo) return false
		if (ambient != other.ambient) return false
		if (bump != other.bump) return false
		if (emissive != other.emissive) return false
		if (metallic != other.metallic) return false
		if (opacity != other.opacity) return false
		if (reflection != other.reflection) return false
		if (roughness != other.roughness) return false
		if (wireFrame != other.wireFrame) return false

		return true
	}


}