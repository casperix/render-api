package casper.render.animation

import casper.geometry.Transform
import casper.render.model.AnimationPlayMode


interface TransformAnimation<Custom : Any> {
	val keys: List<AnimationKey<Custom>>
	fun execute(source: Transform, frame: Double, mode: AnimationPlayMode): Transform

	val startTime get() = keys.first().time
	val finishTime get() = keys.last().time
	val duration get() = finishTime - startTime
}