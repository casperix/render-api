package casper.render

import casper.render.material.MaterialReference
import casper.render.model.SceneModel
import casper.render.model.SceneNode
import casper.render.vertex.VerticesReference


data class SceneData(
		val name: String,
		val verticesList: List<VerticesReference>,
		val materialList: List<MaterialReference>,
		val nodeList: List<SceneNode>,
		val animationList: List<NodeAnimationGroup>,
		val model: SceneModel
)