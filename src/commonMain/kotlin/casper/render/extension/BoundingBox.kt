package casper.render.extension

import casper.geometry.Vector3d
import casper.geometry.basis.Box3d
import casper.render.vertex.Vertices

object BoundingBox {
	fun calculate(vertices: Vertices): Box3d {
		var boxMax = Vector3d(Double.MIN_VALUE)
		var boxMin = Vector3d(Double.MAX_VALUE)
		vertices.forEach { vertex ->
			boxMax = boxMax.upper(vertex.position)
			boxMin = boxMin.lower(vertex.position)
		}
		return Box3d(boxMin, boxMax)
	}
}