package casper.render

import casper.render.model.SceneNode
import casper.render.model.TimeLine

data class NodeAnimation(val node: SceneNode, val timeLine: TimeLine)

