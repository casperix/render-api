package casper.render.model

data class ModelCopySetting(
		val shareVertices: Boolean = true,
		val shareMaterials: Boolean = true,
		val shareAnimation: Boolean = false
)